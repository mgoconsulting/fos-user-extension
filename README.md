# **MGO FOS User Extensions Symfony Bundle**
**Custom extensions for [FriendsOfSymfony/FOSUserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle).**

**IMPORTANT NOTE**: The FOSUserBundle package only receives minimal maintenance.


___________________

## Installation

#### Installation with composer
```sh
composer config repositories.mgoconsulting/fos-user-extension git https://bitbucket.org/mgoconsulting/fos-user-extension.git
composer require mgoconsulting/fos-user-extension
```
#### Enable the bundle

In *config/bundles.php*
```php
<?php
return [
    ...
    Mgo\FOSUserExtension\FOSUserExtensionBundle::class => ['all' => true],
];
```


## Unique login

This authentication extension prevents that each user has more than one active session simultaneously.

To work properly, this extension needs to be on top of the FOSUserBundle listener et user provider system, with sessions enabled.

You also need to create a specific field to store the current session id:
```php
<?php

namespace Mgo\AppBundle\Entity;

use FOS\UserBundle\Model\User as FOSUser;

class User extends FOSUser
{
...
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $currentSession;
    
    // don't forget getters and setters
}

```

Configuration in *config/packages/mgo_fos_extension.yaml*:
```yaml
mgo_fos_extension:
    unique_login:
        # just enabled or disable it
        enabled: true
        # (optional) name of the specific field to store the current session id
        session_field: currentSession
        # (optional) Expression language format
        # Restrictions to enable/disable unique login on specific roles
        restrictions: 'not(user.isAdmin())'
```
