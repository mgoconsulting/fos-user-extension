<?php

namespace Mgo\FOSUserExtension\DependencyInjection\Compiler;

use Mgo\FOSUserExtension\EventListener\SwitchUserListener;
use Mgo\FOSUserExtension\Security\UserProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

class FosUserCompilerPass implements CompilerPassInterface
{
    public const ID_SUFFIX = '.overwritten_by_mgo';

    public function process(ContainerBuilder $container)
    {
        // search for providers in security config
        if (
            $container->hasExtension('security')
            && $container->hasParameter('mgo_fos_extension.unique_login.config')
        ) {
            $uniqueLoginConfig = $container->getParameter('mgo_fos_extension.unique_login.config');
            if ($uniqueLoginConfig['enabled'] ?? false) {
                $securities = $container->getExtensionConfig('security');
                foreach ($securities as $security) {
                    // override user providers
                    foreach (($security['providers'] ?? []) as $providers) {
                        // change DI of each FOS user providers
                        if ($container->hasDefinition($providers['id'])) {
                            $oldDefinition = $container->getDefinition($providers['id']);
                            if (0 === strpos($oldDefinition->getClass(), 'FOS\UserBundle\Security')) {
                                $container->removeDefinition($providers['id']);
                                $container->setDefinition(
                                    $oldId = $providers['id'] . self::ID_SUFFIX,
                                    $oldDefinition
                                );

                                $newDefinition = new Definition(
                                    UserProvider::class,
                                    [
                                        new Reference($oldId),
                                        new Reference('session'),
                                        new Reference('logger'),
                                        new Parameter('mgo_fos_extension.unique_login.config'),
                                    ]
                                );
                                $container->setDefinition($providers['id'], $newDefinition);
                            }
                        }
                    }

                    // override switch user listeners
                    $switchuserListener = 'security.authentication.switchuser_listener';
                    if ($container->hasDefinition($switchuserListener)) {
                        $definition = $container->getDefinition($switchuserListener);
                        $definition->setClass(SwitchUserListener::class);
                        $definition->addMethodCall('setSession', [new Reference('session')]);
                        $container->setDefinition(
                            $switchuserListener,
                            $definition
                        );
                    }
                }
            }
        }
    }
}
