<?php

namespace Mgo\FOSUserExtension\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Bundle Configuration.
 */
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(); // @phpstan-ignore-line
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root('mgo_fos_extension'); // @phpstan-ignore-line
        }

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $root */
        $root
            ->children()
                ->arrayNode('unique_login')
                    ->canBeDisabled()
                    ->children()
                        ->scalarNode('session_field')
                            ->defaultValue('session')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('restrictions')
                            ->defaultNull()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
