<?php

namespace Mgo\FOSUserExtension\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class MgoFOSUserExtension extends Extension
{
    public function getAlias()
    {
        return 'mgo_fos_extension';
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        // unique_login config as parameter
        $container->setParameter(
            'mgo_fos_extension.unique_login.config',
            $config['unique_login']
        );
        if ($config['unique_login']['enabled'] ?? false) {
            $loader->load('listeners.yml');
        }
    }
}
