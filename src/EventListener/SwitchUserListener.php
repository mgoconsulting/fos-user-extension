<?php

namespace Mgo\FOSUserExtension\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Http\Firewall\SwitchUserListener as BaseSwitchUserListener;

class SwitchUserListener extends BaseSwitchUserListener
{
    public const SESSION_NAME = 'switch_user';

    private $session;
    private $usernameParameter;

    public function __construct()
    {
        $args = func_get_args();

        $this->usernameParameter = $args[6];

        call_user_func_array([$this, 'parent::' . __FUNCTION__], $args);
    }

    public function setSession(Session $session): void
    {
        $this->session = $session;
    }

    public function handle(GetResponseEvent $event)
    {
        // save username first before calling parent method.
        $request = $event->getRequest();
        $username = $request->get($this->usernameParameter) ?: $request->headers->get($this->usernameParameter);

        parent::handle($event);

        if (!$username) {
            return;
        }

        // set we are in a swicth user mode in session
        // used in:
        // @see \Mgo\FOSUserExtension\Security\UserProvider::refreshUser()
        if (self::EXIT_VALUE === $username) {
            $this->session->remove(self::SESSION_NAME);
        } else {
            $this->session->set(self::SESSION_NAME, $username);
        }
        $this->session->save();
    }
}
