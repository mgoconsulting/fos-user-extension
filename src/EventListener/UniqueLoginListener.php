<?php

namespace Mgo\FOSUserExtension\EventListener;

use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class UniqueLoginListener implements EventSubscriberInterface
{
    /** @var UserManagerInterface */
    private $userManager;

    /** @var Session */
    private $session;

    /** @var array */
    private $uniqueLoginConfig;

    /** @var \Symfony\Component\PropertyAccess\PropertyAccessorInterface */
    private $pa;

    public function __construct(
        UserManagerInterface $userManager,
        Session $session,
        array $uniqueLoginConfig
    ) {
        $this->userManager = $userManager;
        $this->session = $session;
        $this->uniqueLoginConfig = $uniqueLoginConfig;
        $this->pa = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->disableMagicCall()
            ->getPropertyAccessor();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
        ];
    }

    public function onImplicitLogin(UserEvent $event): void
    {
        $this->saveUserSession($event->getUser());
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event): void
    {
        /** @var UserInterface $user */
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof UserInterface) {
            $this->saveUserSession($user);
        }
    }

    private function saveUserSession(UserInterface $user): void
    {
        $this->pa->setValue(
            $user,
            $this->uniqueLoginConfig['session_field'],
            $this->session->getId()
        );
        $this->userManager->updateUser($user);
    }
}
