<?php

namespace Mgo\FOSUserExtension;

use Mgo\FOSUserExtension\DependencyInjection\Compiler\FosUserCompilerPass;
use Mgo\FOSUserExtension\DependencyInjection\MgoFOSUserExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * MGO FOS User Unique Login Extension Bundle.
 */
class FOSUserExtensionBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new FosUserCompilerPass());
    }

    protected function getContainerExtensionClass()
    {
        return MgoFOSUserExtension::class;
    }
}
