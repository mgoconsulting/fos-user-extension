<?php

namespace Mgo\FOSUserExtension\Security;

use Mgo\FOSUserExtension\EventListener\SwitchUserListener;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Exception\SessionUnavailableException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class UserProvider implements UserProviderInterface
{
    /** @var UserProviderInterface */
    private $userProvider;

    /** @var Session */
    private $session;

    /** @var Logger */
    private $logger;

    /** @var array */
    private $uniqueLoginConfig;

    /** @var \Symfony\Component\PropertyAccess\PropertyAccessorInterface */
    private $pa;

    /** @var ExpressionLanguage */
    private $el;

    public function __construct(
        UserProviderInterface $userProvider,
        Session $session,
        Logger $logger,
        array $uniqueLoginConfig
    ) {
        $this->userProvider = $userProvider;
        $this->session = $session;
        $this->logger = $logger;
        $this->uniqueLoginConfig = $uniqueLoginConfig;
        $this->pa = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->disableMagicCall()
            ->getPropertyAccessor();
        $this->el = new ExpressionLanguage();
    }

    public function loadUserByUsername($username)
    {
        return $this->userProvider->loadUserByUsername($username);
    }

    public function refreshUser(UserInterface $user)
    {
        $reloadedUser = $this->userProvider->refreshUser($user);

        // check restrictions
        if (
            !$this->uniqueLoginConfig['restrictions']
            ||
            $this->el->evaluate(
                $this->uniqueLoginConfig['restrictions'],
                [
                    'user' => $reloadedUser,
                    'session' => $this->session,
                ]
            )
        ) {
            $currentId = $this->pa->getValue($reloadedUser, $this->uniqueLoginConfig['session_field']);
            if (
                !$this->session->has(SwitchUserListener::SESSION_NAME)
                && $currentId
                && $currentId !== $this->session->getId()
            ) {
                $message = "Multiple logins for same user detected ({$reloadedUser->getUsername()}).";
                $this->logger->alert($message);
                throw new SessionUnavailableException($message, 403);
            }
        }

        return $reloadedUser;
    }

    public function supportsClass($class)
    {
        return $this->userProvider->supportsClass($class);
    }
}
